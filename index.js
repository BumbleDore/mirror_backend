require("dotenv").config();
const express = require("express");
const port = 3002;
const fs = require("fs");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const writer = require("./test");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const https = require("https");
const axios = require("axios");

const options = {
  key: fs.readFileSync("private.key"),
  cert: fs.readFileSync("certificate.crt"),
};

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(
  cors({
    origin: "*",
  })
);

app.get("/", (request, response) => {
  console.log(`URL: ${request.url}`);
  response.send("Hello, Server!");
  writer.hi();
});

app.post("/add_cred", async (req, res) => {
  const { creds } = req.body;
  await creds.forEach((cred) => {
    const { cred_num, cred_value } = cred;
    if (!cred_num || !cred_value) {
      return res.status(404).send("Make sure all details are provided");
    }
    writer.add_cred(cred_num, cred_value);
  });
  res.send({ res: true });
});

app.get("/get_cals", async (req, res) => {
  const cals = await writer.get_calendars();
  res.send({ res: cals });
});

app.post("/add_cal", async (req, res) => {
  const { url, name } = req.body;
  console.log(url, name);
  if (!url || !name) {
    return res.status(404).send("Make sure all details are provided");
  }
  console.log("iugihg");
  writer.add_cal(url, name);
  res.send({ res: "" });
});

app.post("/remove_cal", async (req, res) => {
  const { id } = req.body;
  if (!id) {
    return res.status(404).send("Make sure all details are provided");
  }
  writer.remove_cal(id);
  res.send({ res: "" });
});

app.get("/start_spotify_auth", async (req, res) => {
  const { stdout, stderr } = await exec("echo hello");
  console.log(stdout, stderr);
  return res.send({ res: "Hello" });
});

app.get("/compliments/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/compliments/toggle"
  );
  res.send(respone.data);
});

app.get("/currentweather/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/currentweather/toggle"
  );
  res.send(respone.data);
});

app.get("/clock/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/clock/toggle"
  );
  res.send(respone.data);
});

app.get("/MMM-network-signal/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/MMM-network-signal/toggle"
  );
  res.send(respone.data);
});

app.get("/newsfeed/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/newsfeed/toggle"
  );
  res.send(respone.data);
});

app.get("/MMM-NowPlayingOnSpotify/toggle", async (req, res) => {
  const respone = await axios.get(
    "http://raspberrypi:8080/api/module/MMM-NowPlayingOnSpotify/toggle"
  );
  res.send(respone.data);
});

app.get("/monitor/toggle", async (req, res) => {
  const respone = await axios.get("http://raspberrypi:8080/api/monitor/toggle");
  res.send(respone.data);
});

app.get("/brightness/${value}", async (req, res) => {
  const respone = await axios.get(
    `http://raspberrypi:8080/api/brightness/${value}`
  );
  res.send(respone.data);
});

var server = https.createServer(options, app);

server.listen(port, (error) => {
  if (error) return console.log(`Error: ${error}`);

  console.log(`Server listening on port ${server.address().port}`);
});
