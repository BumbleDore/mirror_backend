const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const config_path = "/home/pi/test/magicmirror/config/config.js";
const add_cred = async (cred_num, cred_value) => {
  const data = fs.readFileSync(config_path, {
    encoding: "utf8",
    flag: "r",
  });

  const s = data.toString();
  console.log(s);
  let s2;
  //switch statement here
  switch (cred_num) {
    case 1:
      s2 = s.replace(/let WEATHER_API=.*/i, `let WEATHER_API="${cred_value}"`);
      break;
    case 2:
      s2 = s.replace(/let WEATHER_LAT=.*/i, `let WEATHER_LAT="${cred_value}"`);
      break;
    case 3:
      s2 = s.replace(
        /let WEATHER_LONG=.*/i,
        `let WEATHER_LONG="${cred_value}"`
      );
      break;
    case 4:
      s2 = s.replace(
        /let WEATHER_CITY=.*/i,
        `let WEATHER_CITY="${cred_value}"`
      );
      break;
    case 5:
      s2 = s.replace(
        /let CHOOSEN_HOLIDAY_CAL=.*/i,
        `let CHOOSEN_HOLIDAY_CAL="${cred_value}"`
      );
      break;
    case 6:
      s2 = s.replace(
        /let SPOT_ACCESS_TOKEN=.*/i,
        `let SPOT_ACCESS_TOKEN="${cred_value}"`
      );
      break;
    case 7:
      s2 = s.replace(
        /let SPOT_REFRESH_TOKEN=.*/i,
        `let SPOT_REFRESH_TOKEN="${cred_value}"`
      );
      break;
    case 8:
      s2 = s.replace(
        /let SPOT_CLIENT_ID=.*/i,
        `let SPOT_CLIENT_ID="${cred_value}"`
      );
      break;
    case 9:
      s2 = s.replace(
        /let SPOT_CLIENT_SECRET=.*/i,
        `let SPOT_CLIENT_SECRET="${cred_value}"`
      );
      break;
  }
  fs.writeFileSync(config_path, s2, (error) => {
    if (error) throw err;
  });
};

const get_calendars = async () => {
  const data = fs.readFileSync(config_path, {
    encoding: "utf8",
    flag: "r",
  });
  console.log(data);
  let currentCals;
  const lines = data.split(/\r?\n/);

  lines.forEach((line) => {
    if (line.startsWith("let calendars")) {
      var json = line.substring(line.indexOf("=") + 1, line.length);
      currentCals = JSON.parse(json);
    }
  });

  const { calendars } = currentCals;

  console.log(currentCals);
  return calendars;
};

const add_cal = async (url, name) => {
  const data = fs.readFileSync(config_path, { encoding: "utf8", flag: "r" });
  let currentCals;
  const lines = data.split(/\r?\n/);

  lines.forEach((line) => {
    if (line.startsWith("let CALENDARS")) {
      var json = line.substring(line.indexOf("=") + 1, line.length);
      currentCals = JSON.parse(json);
    }
  });

  const { CALENDARS } = currentCals;

  CALENDARS.push({
    id: uuidv4(),
    name,
    url,
    symbol: "calendar-check",
  });
  let s2;
  let s = data.toString();
  let writeCal = {
    CALENDARS,
  };
  s2 = s.replace(
    /let CALENDARS=.*/i,
    `let CALENDARS="${JSON.stringify(writeCal)}"`
  );
  fs.writeFileSync(config_path, s2, (error) => {
    if (error) throw err;
  });
};

const remove_cal = async (id) => {
  const data = fs.readFileSync(config_path, { encoding: "utf8", flag: "r" });
  let currentCals;
  const lines = data.split(/\r?\n/);

  lines.forEach((line) => {
    if (line.startsWith("let CALENDARS")) {
      var json = line.substring(line.indexOf("=") + 1, line.length);
      currentCals = JSON.parse(json);
    }
  });

  const { CALENDARS } = currentCals;

  const newCal = CALENDARS.filter((cal) => {
    if (cal.id !== id) {
      return cal;
    }
  });

  let s2;
  let s = data.toString();
  let writeCal = {
    CALENDARS: newCal,
  };
  s2 = s.replace(
    /let CALENDARS=.*/i,
    `let CALENDARS="${JSON.stringify(writeCal)}"`
  );
  fs.writeFileSync(config_path, s2, (error) => {
    if (error) throw err;
  });
};

function hi() {
  fs.readFile("../.bashrc", "utf-8", (err, data) => {
    console.log("Hi");
  });
}

module.exports = { hi, add_cred, get_calendars, add_cal, remove_cal };
